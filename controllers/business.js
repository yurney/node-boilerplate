/**
 * GET /signup
 * Signup page.
 */

exports.getSignup = function(req, res) {
  //if (req.user) return res.redirect('/');
  res.render('business/signup', {
    title: req.i18n.__("Register your business"),
    email: req.i18n.__("Email"),
    password: req.i18n.__("Password"),
    confirm_password: req.i18n.__("Confirm Password"),
    signup: req.i18n.__("Sign up")
  });
};